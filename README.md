# Kode Sumber Aplikasi Belajar Penarapan RESTful API dengan Android Achitecture Component.

Kode sumber aplikasi ini digunakan sebagai catatan hasil belajar tentang bagaimana menerapkan Android Architechture Component.
Selain itu dapat digunakan rebagai referensi teman-teman yang ingin belajar tentang Android Architechture Component.
Preview aplikasi dapat diunduh di Google Play Store pada link berikut ini : [Learning API Consume](https://play.google.com/store/apps/details?id=c.m.apiconsume)

### Komponen utama yang digunakan :
* Kotlin
* ViewModel
* LiveData
* Room
* Koin
* Paging
* Retrofit
* Gson
* Kotlinx Coroutine
* Material Design

Komponen utama banyak menggunakan komponen dasar dari Android Architechture Component yang merupakan bagian dari [Jetpack](https://developer.android.com/jetpack/?gclid=CjwKCAjw5fzrBRASEiwAD2OSV7yHk7Lerstz_kb1m9XB7Ki92-umRr7poYFhE1g43HPQxmp_VozddBoCwXoQAvD_BwE).
Namun saya tidak memasukkan semuanya, saya meninggalkan komponen [Navigation](https://developer.android.com/topic/libraries/architecture/navigation.html), [Data Binding](https://developer.android.com/topic/libraries/data-binding/), [Work Manager](https://developer.android.com/topic/libraries/architecture/workmanager).
karena aplikasi ini hanya bertujuan untuk membuat aplikasi CRUD sederhana.

### Komponen tambahan untuk analitycs dan adsense (karena saya juga butuh dana pemasukan hehehe ^_^ )
* Firebase Analytics
* Google AdMob

Kode sumber aplikasi ini Bebas dan Terbuka. Saya harap teman-teman jika ada masukkan terhadap penulisan kode atau hal lainnya mohon untuk dituliskan ke ISSUES.

Terima Kasih. Happy Coding.