package c.m.apiconsume.screen

import c.m.apiconsume.R
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KButton

open class PostActivityScreen : Screen<PostActivityScreen>() {
    /* Title and Description Edit Text */
    val titleEditText: KEditText = KEditText { withId(R.id.et_title_event) }
    val descriptionEditText: KEditText = KEditText { withId(R.id.et_description_event) }

    /* Item Menu Save */
    val saveMenu: KButton = KButton { withId(R.id.menu_save) }
}