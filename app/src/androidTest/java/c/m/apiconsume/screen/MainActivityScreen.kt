package c.m.apiconsume.screen

import android.view.View
import c.m.apiconsume.R
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import org.hamcrest.Matcher

open class MainActivityScreen : Screen<MainActivityScreen>() {
    /*
    * RecylerViewScreen
    * */
    val recyclerView: KRecyclerView = KRecyclerView({
        withId(R.id.rv_event)
    }, itemTypeBuilder = {
        itemType(::Item)
    })

    class Item(parent: Matcher<View>) : KRecyclerItem<Item>(parent) {
        val title: KTextView = KTextView(parent) { withId(R.id.tv_event_title) }
        val description: KTextView = KTextView(parent) { withId(R.id.tv_event_description) }
    }

    /* Menu Item */
    val postMenu: KButton = KButton { withId(R.id.menu_post) }
}