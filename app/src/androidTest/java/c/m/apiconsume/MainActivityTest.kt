package c.m.apiconsume

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import c.m.apiconsume.screen.MainActivityScreen
import c.m.apiconsume.ui.main.MainActivity
import com.agoda.kakao.screen.Screen.Companion.onScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {
    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun test() {
        /*
        * This test is done in a state connected to the internet.
        * */
        onScreen<MainActivityScreen> {
            /* Recyclerview test with data available minimum have 5 data */
            recyclerView {
                isVisible()

                firstChild<MainActivityScreen.Item> {
                    isVisible()
                    title { isVisible() }
                    description { isVisible() }
                }

                lastChild<MainActivityScreen.Item> {
                    isVisible()
                    title { isVisible() }
                    description { isVisible() }
                }

                scrollToEnd()
                scrollToStart()
                scrollTo(5)

                childAt<MainActivityScreen.Item>(5) {
                    isClickable()
                    click()
                }
            }

            /* Back to main activity */
            pressBack()

            /* Post Menu is visible */
            postMenu {
                isVisible()
                isClickable()
                click()
            }

            /* Back to main activity */
            pressBack()
        }
    }

}