package c.m.apiconsume

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import c.m.apiconsume.screen.PostActivityScreen
import c.m.apiconsume.ui.post.PostActivity
import com.agoda.kakao.screen.Screen.Companion.onScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class PostActivityTest {
    @Rule
    @JvmField
    val rule = ActivityTestRule(PostActivity::class.java)

    @Test
    fun test() {
        /*
        * This test is done in a state connected to the internet.
        * */
        onScreen<PostActivityScreen> {
            /* Edit Test is visible */
            titleEditText {
                isVisible()
                typeText("Instrument TEST")
                hasText("Instrument TEST")
            }

            descriptionEditText {
                isVisible()
                typeText("Instrument TEST Description")
                hasText("Instrument TEST Description")
            }

            /* Save Menu is visible */
            saveMenu {
                isVisible()
                isClickable()
                click()
            }
        }
    }
}