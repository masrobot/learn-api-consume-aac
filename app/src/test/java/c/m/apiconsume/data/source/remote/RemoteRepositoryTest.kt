package c.m.apiconsume.data.source.remote

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import c.m.apiconsume.di.repositoryModule
import c.m.apiconsume.di.utilModule
import c.m.apiconsume.util.LiveDataTestUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RemoteRepositoryTest : KoinTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    private val remoteRepository by inject<RemoteRepository>()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(mainThreadSurrogate)
        startKoin {
            modules(listOf(utilModule, repositoryModule))
        }
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        Dispatchers.resetMain()
        stopKoin()
    }

    @Test
    @Throws(Exception::class)
    fun testGetEventsFromRemoteNotNull() = runBlocking {
        val eventList = remoteRepository.getEventsAsLiveData()
        Assert.assertNotNull(LiveDataTestUtil.getValue(eventList))
    }

    @Test
    @Throws(Exception::class)
    fun testCreateEventFromRemoteNotNull() = runBlocking {
        val doCreateEvent =
            remoteRepository.createEventAsLiveData("Test Post Title", "Test Post Description")
        Assert.assertNotNull(LiveDataTestUtil.getValue(doCreateEvent))
    }

    @Test
    @Throws(Exception::class)
    fun testUpdateEventFromRemoteNotNull() = runBlocking {
        val doUpdateEvent =
            remoteRepository.updateEventAsLiveData("XUTlQU9AyfYkbyFu3E2M","Test Post Title Edited", "Test Post Description Edited")
        Assert.assertNotNull(LiveDataTestUtil.getValue(doUpdateEvent))
    }

    @Test
    @Throws(Exception::class)
    fun testDeleteEventFromRemoteNotNull() = runBlocking {
        val doDeleteEvent =
            remoteRepository.deleteEventAsLiveData("jvyiJFvjbHFwRowU7Dwn")
        Assert.assertNotNull(LiveDataTestUtil.getValue(doDeleteEvent))
    }
}