package c.m.apiconsume.di

import androidx.room.Room
import c.m.apiconsume.data.source.ApiConsumeRepository
import c.m.apiconsume.data.source.local.LocalRepository
import c.m.apiconsume.data.source.local.room.EventDatabase
import c.m.apiconsume.data.source.remote.RemoteRepository
import c.m.apiconsume.data.source.remote.webservice.ClientServices
import c.m.apiconsume.ui.detail.DetailViewModel
import c.m.apiconsume.ui.main.MainViewModel
import c.m.apiconsume.ui.post.PostViewModel
import c.m.apiconsume.ui.update.UpdateViewModel
import c.m.apiconsume.util.ContextProviders
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { PostViewModel(get()) }
    viewModel { UpdateViewModel(get()) }
    viewModel { DetailViewModel(get()) }
}

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            EventDatabase::class.java,
            "Event Database"
        ).fallbackToDestructiveMigration()
            .build()
    }
    single { get<EventDatabase>().eventDao() }
}

val utilModule = module {
    single { ClientServices() }
    single { ContextProviders() }
}

val repositoryModule = module {
    single { RemoteRepository(get()) }
    single { LocalRepository(get()) }
    single { ApiConsumeRepository(get(), get(), get(), androidContext()) }
}