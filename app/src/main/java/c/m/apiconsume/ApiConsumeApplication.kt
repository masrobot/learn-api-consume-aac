package c.m.apiconsume

import android.app.Application
import c.m.apiconsume.di.databaseModule
import c.m.apiconsume.di.repositoryModule
import c.m.apiconsume.di.utilModule
import c.m.apiconsume.di.viewModelModule
import com.google.android.gms.ads.MobileAds
import com.google.firebase.analytics.FirebaseAnalytics
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class ApiConsumeApplication : Application() {


    override fun onCreate() {
        super.onCreate()

        // Firebase and ads Initialize
        FirebaseAnalytics.getInstance(this@ApiConsumeApplication)
        MobileAds.initialize(this@ApiConsumeApplication, getString(R.string.ads))


        // Koin
        startKoin {
            androidLogger()
            androidContext(this@ApiConsumeApplication)
            modules(
                listOf(
                    viewModelModule,
                    databaseModule,
                    utilModule,
                    repositoryModule
                )
            )
        }
    }
}