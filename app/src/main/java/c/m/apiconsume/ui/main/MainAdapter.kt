package c.m.apiconsume.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import c.m.apiconsume.R
import c.m.apiconsume.data.source.local.entity.EventEntity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_event.*

class MainAdapter(private val onClickListener: (EventEntity) -> Unit) :
    PagedListAdapter<EventEntity, MainAdapter.EventViewHolder>(DiffCallBack) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder =
        EventViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_event, parent, false)
        )

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val contents = getItem(position)
        if (contents != null) holder.bind(contents, onClickListener)
    }

    class EventViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(contentEntity: EventEntity, onClickListener: (EventEntity) -> Unit) {
            item_event_card.setOnClickListener { onClickListener(contentEntity) }

            tv_event_title.text = contentEntity.eventTitle
            tv_event_description.text = contentEntity.eventDescription
        }
    }

    companion object DiffCallBack : DiffUtil.ItemCallback<EventEntity>() {
        override fun areItemsTheSame(oldItem: EventEntity, newItem: EventEntity): Boolean =
            oldItem.idEventTable == newItem.idEventTable

        override fun areContentsTheSame(oldItem: EventEntity, newItem: EventEntity): Boolean =
            oldItem == newItem
    }
}