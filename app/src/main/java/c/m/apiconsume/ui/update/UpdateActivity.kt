package c.m.apiconsume.ui.update

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import c.m.apiconsume.R
import c.m.apiconsume.data.source.local.entity.EventEntity
import c.m.apiconsume.util.alertDialog
import c.m.apiconsume.util.isNetworkStatusAvailable
import c.m.apiconsume.util.toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import kotlinx.android.synthetic.main.activity_update.*
import org.koin.android.ext.android.inject

class UpdateActivity : AppCompatActivity() {
    private val updateViewModel: UpdateViewModel by inject()
    private lateinit var event: EventEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)

        // AdMob Configure
        val adView = findViewById<AdView>(R.id.ads_update)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        // Load event data from parcelable
        event = intent.getParcelableExtra(EXTRA_EVENT) as EventEntity

        et_title_event.setText(event.eventTitle)
        et_description_event.setText(event.eventDescription)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.save_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.menu_save -> {
                // Get data from edit text and intent bundle
                val idEvent = event.eventId.toString()
                val titleEvent = et_title_event.text.toString()
                val descriptionEvent = et_description_event.text.toString()

                // Form Validation
                // for title
                et_title_event.nonEmpty {
                    et_title_event.error = it
                }
                // for description
                et_description_event.nonEmpty {
                    et_description_event.error = it
                }

                // Check form data
                if (et_title_event.nonEmpty() && et_description_event.nonEmpty()) {
                    // Check is online
                    if (this.isNetworkStatusAvailable()) {
                        // this alert dialog for safety, make sure what you are update
                        alertDialog(this, getString(R.string.update_event_alert))?.apply {
                            setPositiveButton(
                                getString(R.string.oke)
                            ) { _, _ ->
                                // Post Event
                                updateViewModel.updateEvent(idEvent, titleEvent, descriptionEvent)

                                // Back to main pack
                                onBackPressed()

                                toast(
                                    this@UpdateActivity,
                                    "Success update event $titleEvent"
                                ).show()
                            }
                            setNegativeButton(
                                getString(R.string.no)
                            ) { _, _ ->
                                toast(this@UpdateActivity, "Cancel update event $titleEvent").show()
                            }
                        }?.create()?.show()
                    } else {
                        // Show alert information if this offline.
                        alertDialog(
                            this,
                            getString(R.string.no_internet_alert)
                        )?.create()?.show()
                    }
                }
            }

        }
        return true
    }

    companion object {
        const val EXTRA_EVENT = "extra_event"
    }
}
