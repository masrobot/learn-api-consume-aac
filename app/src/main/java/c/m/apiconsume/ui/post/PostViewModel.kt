package c.m.apiconsume.ui.post

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.m.apiconsume.data.source.ApiConsumeRepository
import c.m.apiconsume.data.source.local.entity.EventEntity
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class PostViewModel(private val apiConsumeRepository: ApiConsumeRepository) : ViewModel() {

    fun postEvent(eventTitle: String, eventDescription: String) {
        val eventEntity = EventEntity(
            eventId = "",
            eventTitle = eventTitle,
            eventDescription = eventDescription
        )

        viewModelScope.launch {
            apiConsumeRepository.postEvent(eventEntity)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}