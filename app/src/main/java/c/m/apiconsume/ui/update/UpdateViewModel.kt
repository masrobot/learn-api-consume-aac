package c.m.apiconsume.ui.update

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import c.m.apiconsume.data.source.ApiConsumeRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class UpdateViewModel(private val apiConsumeRepository: ApiConsumeRepository) : ViewModel() {

    fun updateEvent(eventId: String, eventTitle: String, eventDescription: String) {
        viewModelScope.launch {
            apiConsumeRepository.updateEventById(eventId, eventTitle, eventDescription)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}