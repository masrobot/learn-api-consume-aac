package c.m.apiconsume.ui.post

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import c.m.apiconsume.R
import c.m.apiconsume.util.alertDialog
import c.m.apiconsume.util.isNetworkStatusAvailable
import c.m.apiconsume.util.toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.wajahatkarim3.easyvalidation.core.view_ktx.nonEmpty
import kotlinx.android.synthetic.main.activity_post.*
import org.koin.android.ext.android.inject

class PostActivity : AppCompatActivity() {
    private val postViewModel: PostViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        // AdMob Configure
        val adView = findViewById<AdView>(R.id.ads_post)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.save_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_save -> {
                // Get data from edit text
                val titleEvent = et_title_event.text.toString()
                val descriptionEvent = et_description_event.text.toString()

                // Form Validation
                // for title
                et_title_event.nonEmpty {
                    et_title_event.error = it
                }
                // for description
                et_description_event.nonEmpty {
                    et_description_event.error = it
                }

                // Check form data
                if (et_title_event.nonEmpty() && et_description_event.nonEmpty()) {
                    // Check is online
                    if (this.isNetworkStatusAvailable()) {
                        // this alert dialog for safety, make sure what you are post
                        alertDialog(this, getString(R.string.posting_event_alert))?.apply {
                            setPositiveButton(
                                getString(R.string.oke)
                            ) { _, _ ->
                                // Post Event
                                postViewModel.postEvent(titleEvent, descriptionEvent)

                                // Back to main pack
                                onBackPressed()

                                // Toast information
                                toast(this@PostActivity, "Success post event $titleEvent").show()
                            }
                            setNegativeButton(
                                getString(R.string.no)
                            ) { _, _ ->
                                toast(this@PostActivity, "Cancel post event $titleEvent").show()
                            }
                        }?.create()?.show()
                    } else {
                        // Show alert information if this offline.
                        alertDialog(
                            this,
                            getString(R.string.no_internet_alert)
                        )?.create()?.show()
                    }
                }
            }
        }
        return true
    }
}
