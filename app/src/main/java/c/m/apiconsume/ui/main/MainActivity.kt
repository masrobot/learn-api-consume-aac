package c.m.apiconsume.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import c.m.apiconsume.R
import c.m.apiconsume.ui.detail.DetailActivity
import c.m.apiconsume.ui.post.PostActivity
import c.m.apiconsume.util.gone
import c.m.apiconsume.util.toast
import c.m.apiconsume.util.visible
import c.m.apiconsume.vo.Status
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val mainViewModel: MainViewModel by viewModel()
    private lateinit var mainAdapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // AdMob Configure
        val adView = findViewById<AdView>(R.id.ads_main)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        // Initiate Adapter Paging
        mainAdapter = MainAdapter {
            val eventWithObjectIntent = Intent(this, DetailActivity::class.java)
                .putExtra(DetailActivity.EXTRA_EVENT, it)
            startActivity(eventWithObjectIntent)
        }

        // Observe data from ViewModel
        observeDataViewModel()

        // Initiate RecyclerView
        rv_event.setHasFixedSize(true)
        rv_event.adapter = mainAdapter

        // refresh page
        swipe_layout_main.setOnRefreshListener {
            swipe_layout_main.isRefreshing = false
            observeDataViewModel()
        }
    }

    private fun observeDataViewModel() {
        mainViewModel.getContentEvents().observe(this, Observer {
            if (it != null) {
                when (it.status) {
                    Status.LOADING -> {
                        progress_bar_main.visible()
                        rv_event.gone()
                        tv_no_data.gone()
                    }
                    Status.SUCCESS -> {
                        progress_bar_main.gone()
                        rv_event.visible()
                        tv_no_data.gone()

                        // Submit Data to Adapter
                        swipe_layout_main.isRefreshing = false
                        mainAdapter.submitList(it.data)
                        mainAdapter.notifyDataSetChanged()

                        // If data from Repository is Null or Empty Array show "No Data" Page
                        if (it.data.isNullOrEmpty()) {
                            rv_event.gone()
                            tv_no_data.visible()
                            toast(this, getString(R.string.no_data)).show()
                        }
                    }
                    Status.ERROR -> {
                        progress_bar_main.gone()
                        rv_event.gone()
                        tv_no_data.visible()
                    }
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_post -> startActivity(Intent(this, PostActivity::class.java))
        }
        return true
    }
}
