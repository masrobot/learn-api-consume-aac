package c.m.apiconsume.ui.detail

import android.util.Log
import androidx.lifecycle.*
import c.m.apiconsume.data.source.ApiConsumeRepository
import c.m.apiconsume.data.source.local.entity.EventEntity
import c.m.apiconsume.vo.Resource
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class DetailViewModel(private val apiConsumeRepository: ApiConsumeRepository) : ViewModel() {

    /*
    * This code format use for Live Data Observable. Read this for reason :
    * https://developer.android.com/topic/libraries/architecture/livedata#transform_livedata
    * Read on "Transformations.switchMap()" Section.
    * */

    private val _id: MutableLiveData<String> = MutableLiveData()
    val getEventById: LiveData<Resource<EventEntity>> = Transformations.switchMap(_id) {
        apiConsumeRepository.getEventById(it)
    }

    fun getEventId(id: String) {
        Log.d("Value", id)
        _id.value = id
    }

    fun deleteEventById(eventId: String) {
        viewModelScope.launch {
            apiConsumeRepository.deleteEventById(eventId)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}