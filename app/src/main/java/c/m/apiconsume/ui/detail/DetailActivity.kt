package c.m.apiconsume.ui.detail

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import c.m.apiconsume.R
import c.m.apiconsume.data.source.local.entity.EventEntity
import c.m.apiconsume.ui.update.UpdateActivity
import c.m.apiconsume.util.*
import c.m.apiconsume.vo.Status
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailActivity : AppCompatActivity() {
    private val detailViewModel: DetailViewModel by viewModel()
    private lateinit var event: EventEntity
    private var idTableEvent: Long? = 0L
    private var eventId: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // AdMob Configure
        val adView = findViewById<AdView>(R.id.ads_detail)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        // Load event data from parcelable
        event = intent.getParcelableExtra(EXTRA_EVENT) as EventEntity
        idTableEvent = event.idEventTable
        eventId = event.eventId.toString()

        // Check event id is not blank string
        if (eventId != "") {
            getEventByIdEvent()
        } else {
            getEventByIdTableEvent()
        }
    }

    private fun getEventByIdTableEvent() {
        detailViewModel.getEventId(idTableEvent.toString())
        detailViewModel.getEventById.observe(this, Observer { eventData ->
            if (eventData != null) {
                when (eventData.status) {
                    Status.LOADING -> {
                        tv_title_event.text = ""
                        tv_description_event.text = ""
                        fab_update_event.gone()
                    }
                    Status.SUCCESS -> {
                        // use data event from source not parcelable
                        tv_title_event.text = eventData.data?.eventTitle
                        tv_description_event.text = eventData.data?.eventDescription

                        fab_update_event.visible()
                        fab_update_event.setOnClickListener {
                            val eventWithObjectIntent = Intent(this, UpdateActivity::class.java)
                                .putExtra(UpdateActivity.EXTRA_EVENT, eventData.data)
                            startActivity(eventWithObjectIntent)
                        }
                    }
                    Status.ERROR -> {
                        tv_title_event.text = getString(R.string.no_data)
                        tv_description_event.text = getString(R.string.no_data)
                        fab_update_event.gone()
                    }
                }
            }
        })
    }

    private fun getEventByIdEvent() {
        detailViewModel.getEventId(eventId.toString())
        detailViewModel.getEventById.observe(this, Observer { eventData ->
            if (eventData != null) {
                when (eventData.status) {
                    Status.LOADING -> {
                        tv_title_event.text = ""
                        tv_description_event.text = ""
                        fab_update_event.gone()
                    }
                    Status.SUCCESS -> {
                        // use data event from source not parcelable
                        tv_title_event.text = eventData.data?.eventTitle
                        tv_description_event.text = eventData.data?.eventDescription

                        fab_update_event.visible()
                        fab_update_event.setOnClickListener {
                            val eventWithObjectIntent = Intent(this, UpdateActivity::class.java)
                                .putExtra(UpdateActivity.EXTRA_EVENT, eventData.data)
                            startActivity(eventWithObjectIntent)
                        }
                    }
                    Status.ERROR -> {
                        tv_title_event.text = getString(R.string.no_data)
                        tv_description_event.text = getString(R.string.no_data)
                        fab_update_event.gone()
                    }
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_delete -> {
                // Check is online
                if (this.isNetworkStatusAvailable()) {
                    // this alert dialog for safety, make sure what you are post
                    alertDialog(
                        this,
                        "Are you sure to delete ${event.eventTitle} ?"
                    )?.apply {
                        setPositiveButton(
                            getString(R.string.oke)
                        ) { _, _ ->
                            // Delete Event
                            detailViewModel.deleteEventById(eventId.toString())

                            // Back to main pack
                            onBackPressed()

                            toast(this@DetailActivity, getString(R.string.data_deleted)).show()
                        }
                        setNegativeButton(
                            getString(R.string.no)
                        ) { _, _ ->
                            toast(
                                this@DetailActivity,
                                getString(R.string.cancel_delete_data)
                            ).show()
                        }
                    }?.create()?.show()
                } else {
                    // Show alert information if this offline.
                    alertDialog(
                        this,
                        getString(R.string.no_internet_alert)
                    )?.create()?.show()
                }
            }
        }
        return true
    }

    companion object {
        const val EXTRA_EVENT = "extra_event"
    }
}
