package c.m.apiconsume.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import c.m.apiconsume.data.source.ApiConsumeRepository
import c.m.apiconsume.data.source.local.entity.EventEntity
import c.m.apiconsume.vo.Resource
import kotlinx.coroutines.cancel

class MainViewModel(private val apiConsumeRepository: ApiConsumeRepository) : ViewModel() {
    fun getContentEvents(): LiveData<Resource<PagedList<EventEntity>>> =
        apiConsumeRepository.getEvents()


    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}