package c.m.apiconsume.vo

enum class Status { SUCCESS, ERROR, LOADING }