package c.m.apiconsume.data.source.local.room

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import c.m.apiconsume.data.source.local.entity.EventEntity

@Dao
interface EventDao {
    // this query for save data from API to SQLite DB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEvents(eventEntity: List<EventEntity>)

    // before insert new data from API, we will clean event_table first
    @Query("DELETE FROM event_table")
    fun deleteEvents()

    // the results of the combined query are as follows
    @Transaction
    fun updateEvents(eventEntity: List<EventEntity>) {
        deleteEvents()
        insertEvents(eventEntity)
    }

    // Delete event by id
    @Query("DELETE FROM event_table WHERE event_id = :eventId")
    suspend fun deleteByEventId(eventId: String)

    // Update event by id
    @Query("UPDATE event_table SET event_title = :eventTitle, event_description = :eventDescription WHERE event_id = :eventId")
    suspend fun updateEventById(eventId: String, eventTitle: String, eventDescription: String)

    // Post new event
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun postEvent(eventEntity: EventEntity)

    // Get All Events
    @Query("SELECT * FROM event_table ORDER BY _id DESC")
    fun getEvents(): DataSource.Factory<Int, EventEntity>

    @Query("SELECT * FROM event_table WHERE _id = :eventId OR event_id = :eventId")
    fun getEventById(eventId: String): LiveData<EventEntity>
}