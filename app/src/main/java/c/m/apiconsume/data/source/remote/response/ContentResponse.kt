package c.m.apiconsume.data.source.remote.response
import com.google.gson.annotations.SerializedName


data class ContentResponse(
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("results")
    val results: List<ResultResponse>,
    @SerializedName("status")
    val status: Int? = null
)

data class ResultResponse(
    @SerializedName("data")
    val `data`: Data? = null,
    @SerializedName("id")
    val id: String? = null
)

data class Data(
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("title")
    val title: String? = null
)