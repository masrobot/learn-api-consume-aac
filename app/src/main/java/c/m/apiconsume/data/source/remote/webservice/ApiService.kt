package c.m.apiconsume.data.source.remote.webservice

import c.m.apiconsume.data.source.remote.response.ContentResponse
import retrofit2.http.*

interface ApiService {
    @GET("events")
    suspend fun getEvents(): ContentResponse

    @GET("event")
    suspend fun getEventById(
        @Query("id") eventId: String
    ): ContentResponse

    @FormUrlEncoded
    @POST("events")
    suspend fun createEvent(
        @Field("title") title: String,
        @Field("description") description: String
    ): ContentResponse

    @FormUrlEncoded
    @PUT("event/update")
    suspend fun updateEvent(
        @Query("id") eventId: String,
        @Field("title") title: String,
        @Field("description") description: String
    ): ContentResponse

    @DELETE("event/delete")
    suspend fun deleteEvent(
        @Query("id") eventId: String
    ): ContentResponse
}