package c.m.apiconsume.data.source.local

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import c.m.apiconsume.data.source.local.entity.EventEntity
import c.m.apiconsume.data.source.local.room.EventDao

class LocalRepository(
    private val eventDao: EventDao
) {
    // Add Data from Webservice
    fun updateEvents(eventEntity: List<EventEntity>) = eventDao.updateEvents(eventEntity)

    // Get All Events Data
    fun getEvents(): DataSource.Factory<Int, EventEntity> = eventDao.getEvents()

    // Get Event by ID
    fun getEventById(eventId: String): LiveData<EventEntity> = eventDao.getEventById(eventId)

    // post New Event
    suspend fun postEvent(eventEntity: EventEntity) = eventDao.postEvent(eventEntity)

    // Update Event by ID
    suspend fun updateEventById(eventId: String, eventTitle: String, eventDescription: String) =
        eventDao.updateEventById(eventId, eventTitle, eventDescription)

    // Delete Event by ID
    suspend fun deleteEventById(eventId: String) = eventDao.deleteByEventId(eventId)
}