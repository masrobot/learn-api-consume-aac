package c.m.apiconsume.data.source.remote.webservice

import c.m.apiconsume.BuildConfig

class ClientServices {
    private val apiService =
        RetrofitService.getInstance(BASE_URL_API).create(ApiService::class.java)

    suspend fun getEvents() = apiService.getEvents()

    suspend fun getEventById(eventId: String) = apiService.getEventById(eventId)

    suspend fun createEvent(titleEvent: String, descriptionEvent: String) =
        apiService.createEvent(titleEvent, descriptionEvent)

    suspend fun updateEvent(eventId: String, titleEvent: String, descriptionEvent: String) =
        apiService.updateEvent(eventId, titleEvent, descriptionEvent)

    suspend fun deleteEvent(eventId: String) = apiService.deleteEvent(eventId)

    companion object {
        const val BASE_URL_API = BuildConfig.BASE_URL_API
    }
}