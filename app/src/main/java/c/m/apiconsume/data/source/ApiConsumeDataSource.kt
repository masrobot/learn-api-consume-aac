package c.m.apiconsume.data.source

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import c.m.apiconsume.data.source.local.entity.EventEntity
import c.m.apiconsume.vo.Resource

interface ApiConsumeDataSource {
    fun getEvents(): LiveData<Resource<PagedList<EventEntity>>>
    fun getEventById(eventId: String): LiveData<Resource<EventEntity>>
    suspend fun postEvent(eventEntity: EventEntity)
    suspend fun updateEventById(eventId: String, eventTitle: String, eventDescription: String)
    suspend fun deleteEventById(eventId: String)
}