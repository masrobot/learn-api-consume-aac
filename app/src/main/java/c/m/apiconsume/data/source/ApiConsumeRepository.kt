package c.m.apiconsume.data.source

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import androidx.paging.toLiveData
import c.m.apiconsume.data.source.local.LocalRepository
import c.m.apiconsume.data.source.local.entity.EventEntity
import c.m.apiconsume.data.source.remote.ApiResponse
import c.m.apiconsume.data.source.remote.RemoteRepository
import c.m.apiconsume.data.source.remote.response.ResultResponse
import c.m.apiconsume.util.ContextProviders
import c.m.apiconsume.util.isNetworkStatusAvailable
import c.m.apiconsume.vo.Resource

class ApiConsumeRepository(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository,
    private val contextProviders: ContextProviders,
    context: Context
) : ApiConsumeDataSource {

    private val isOnline = context.isNetworkStatusAvailable()

    /* Get Events from API/Source and save to SQLite DB for offline usage */
    override fun getEvents(): LiveData<Resource<PagedList<EventEntity>>> = object :
        NetworkBoundResource<PagedList<EventEntity>, List<ResultResponse>>(
            contextProviders
        ) {
        /* Get Data from API/Source */
        override suspend fun createCall(): LiveData<ApiResponse<List<ResultResponse>>> =
            remoteRepository.getEventsAsLiveData()

        /*
        * I am make it always "true" for get data from network if device connected to the internet
        * if the device not connected to the internet, load data from local database.
        */
        override fun shouldFetch(data: PagedList<EventEntity>?): Boolean =
            data == null || data.isEmpty() || isOnline

        /* Get data from SQLite DB */
        override fun loadFromDb(): LiveData<PagedList<EventEntity>> =
            localRepository.getEvents().toLiveData(pageSize = 5, initialLoadKey = 1)

        /* return data from API/Source to SQLite DB */
        override suspend fun saveCallResult(item: List<ResultResponse>) {
            val eventEntities: ArrayList<EventEntity> = arrayListOf()

            for (resultResponse in item) {
                eventEntities.add(
                    EventEntity(
                        eventId = resultResponse.id.toString(),
                        eventTitle = resultResponse.data?.title.toString(),
                        eventDescription = resultResponse.data?.description.toString()
                    )
                )
            }

            localRepository.updateEvents(eventEntities)
        }

        /* Fetch Failed LOG error message */
        override fun onFetchFailed(message: String?) {
            Log.e(LOG_TITLE, message.toString())
        }
    }.asLiveData()

    override fun getEventById(eventId: String): LiveData<Resource<EventEntity>> = object :
        NetworkBoundResource<EventEntity, List<ResultResponse>>(contextProviders) {
        /* Get data from SQLite DB */
        override fun loadFromDb(): LiveData<EventEntity> = localRepository.getEventById(eventId)

        /*
        * Check data from local first, if no data get data from API/Source on createCall() function,
        * but if SQLite DB has data get data from SQLite DB.
        * */
        override fun shouldFetch(data: EventEntity?): Boolean = false

        /* Get Data from API/Source */
        override suspend fun createCall(): LiveData<ApiResponse<List<ResultResponse>>>? = null

        /* return data from API/Source to SQLite DB */
        override suspend fun saveCallResult(item: List<ResultResponse>) {}

        /* Fetch Failed LOG error message */
        override fun onFetchFailed(message: String?) {
            Log.e(LOG_TITLE, message.toString())
        }
    }.asLiveData()


    override suspend fun postEvent(eventEntity: EventEntity) {
        /* Save data to SQLite */
        localRepository.postEvent(eventEntity)

        /* Save data to server API/Source */
        remoteRepository.createEventAsLiveData(
            eventEntity.eventTitle.toString(),
            eventEntity.eventDescription.toString()
        )
    }

    override suspend fun updateEventById(
        eventId: String,
        eventTitle: String,
        eventDescription: String
    ) {
        /* Save data to SQLite DB */
        localRepository.updateEventById(eventId, eventTitle, eventDescription)

        /* Save data to server API/Source */
        remoteRepository.updateEventAsLiveData(eventId, eventTitle, eventDescription)
    }

    /* Delete event by id from SQLite DB and API/Source */
    override suspend fun deleteEventById(eventId: String) {
        localRepository.deleteEventById(eventId)
        remoteRepository.deleteEventAsLiveData(eventId)
    }

    companion object {
        const val LOG_TITLE = "ApiConsumeRepositoryLog"
    }
}