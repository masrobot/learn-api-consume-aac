package c.m.apiconsume.data.source.remote

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import c.m.apiconsume.data.source.remote.response.ResultResponse
import c.m.apiconsume.data.source.remote.webservice.ClientServices

class RemoteRepository(private val clientServices: ClientServices) {
    suspend fun getEventsAsLiveData(): LiveData<ApiResponse<List<ResultResponse>>> {
        val eventResult: MutableLiveData<ApiResponse<List<ResultResponse>>> = MutableLiveData()

        try {
            val clientServiceGetEvents = clientServices.getEvents().results

            if (clientServiceGetEvents.isNullOrEmpty()) {
                eventResult.value = ApiResponse.empty("empty data", clientServiceGetEvents)
            } else {
                eventResult.value = ApiResponse.success(clientServiceGetEvents)
            }

        } catch (e: Exception) {
            Log.e("Error", e.localizedMessage as String)
            eventResult.value = ApiResponse.error("error : ${e.localizedMessage}", null)
        }

        return eventResult
    }

    suspend fun createEventAsLiveData(
        eventTitle: String,
        eventDescription: String
    ): LiveData<ApiResponse<List<ResultResponse>>> {
        val eventResult: MutableLiveData<ApiResponse<List<ResultResponse>>> = MutableLiveData()
        try {
            val clientServiceCreateEvent =
                clientServices.createEvent(eventTitle, eventDescription).results

            if (clientServiceCreateEvent.isNullOrEmpty()) {
                eventResult.value = ApiResponse.empty("data empty", clientServiceCreateEvent)
            } else {
                eventResult.value = ApiResponse.success(clientServiceCreateEvent)
            }
        } catch (e: Exception) {
            Log.e("Error", e.localizedMessage as String)
            eventResult.value = ApiResponse.error("error : ${e.localizedMessage}", null)
        }
        return eventResult
    }

    suspend fun updateEventAsLiveData(
        eventId: String,
        eventTitle: String,
        eventDescription: String
    ): LiveData<ApiResponse<List<ResultResponse>>> {
        val eventResult: MutableLiveData<ApiResponse<List<ResultResponse>>> = MutableLiveData()
        try {
            val clientServiceUpdateEvent =
                clientServices.updateEvent(eventId, eventTitle, eventDescription).results

            if (clientServiceUpdateEvent.isNullOrEmpty()) {
                eventResult.value = ApiResponse.empty("data empty", clientServiceUpdateEvent)
            } else {
                eventResult.value = ApiResponse.success(clientServiceUpdateEvent)
            }
        } catch (e: Exception) {
            Log.e("Error", e.localizedMessage as String)
            eventResult.value = ApiResponse.error("error : ${e.localizedMessage}", null)
        }
        return eventResult
    }

    suspend fun deleteEventAsLiveData(
        eventId: String
    ): LiveData<ApiResponse<List<ResultResponse>>> {
        val eventResult: MutableLiveData<ApiResponse<List<ResultResponse>>> = MutableLiveData()
        try {
            val clientServiceDeleteEvent =
                clientServices.deleteEvent(eventId).results

            if (clientServiceDeleteEvent.isNullOrEmpty()) {
                eventResult.value = ApiResponse.empty("data empty", clientServiceDeleteEvent)
            } else {
                eventResult.value = ApiResponse.success(clientServiceDeleteEvent)
            }
        } catch (e: Exception) {
            Log.e("Error", e.localizedMessage as String)
            eventResult.value = ApiResponse.error("error : ${e.localizedMessage}", null)
        }
        return eventResult
    }
}