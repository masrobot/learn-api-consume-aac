package c.m.apiconsume.data.source.remote

enum class ApiStatusResponse { SUCCESS, EMPTY, ERROR }