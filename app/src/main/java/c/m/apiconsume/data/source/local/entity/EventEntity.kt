package c.m.apiconsume.data.source.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "event_table")
data class EventEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    val idEventTable: Long = 0L,
    @ColumnInfo(name = "event_id")
    val eventId: String? = "",
    @ColumnInfo(name = "event_title")
    val eventTitle: String? = "",
    @ColumnInfo(name = "event_description")
    val eventDescription: String? = ""
): Parcelable